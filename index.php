<?php require_once "./code.php"; ?>

<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<title>S4-a1: Access Modifiers and Encapsulation</title>
</head>
<body>
	<h1>Building</h1>

	<p>The name of the building is <?= $building->getName(); ?></p>
	<p>The <?= $building->getName(); ?> has <?= $building->getFloors(); ?> floors</p>
	<p>The <?= $building->getName(); ?> is located at <?= $building->getAddress(); ?></p>
	<p><?= $building->setName("Caswynn Complex"); ?></p>

	<p><?= $building->printName(); ?></p>


	
	<h1>Condominium</h1>
	
	<p>The name of the condominium is <?= $condominium->getName(); ?></p>
	<p>The <?= $condominium->getName(); ?> has <?= $condominium->getFloors(); ?> floors</p>
	<p>The <?= $condominium->getName(); ?> is located at <?= $condominium->getAddress(); ?></p>
	<p><?= $condominium->setName("Enzo Tower"); ?></p>

	<p><?= $condominium->printName(); ?></p>

</body>
</html>
